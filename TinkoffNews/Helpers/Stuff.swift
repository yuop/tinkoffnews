//
//  Stuff.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 19.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

struct RefreshControlTitles {
    static let loading = NSAttributedString(string: "Грузимся",attributes: AttributesForString.yellowLightFont)
    static let networkError = NSAttributedString(string: "С интернетом что-то не так",attributes: AttributesForString.yellowLightFont)
}

struct AttributesForString {
    
    static var yellowLightFont:[String:Any] = [NSFontAttributeName : UIFont(name: "Raleway-Light", size: CGFloat(17.0))!, NSForegroundColorAttributeName : UIColor.yellow, NSBackgroundColorAttributeName: UIColor.clear]
    static var yellowMediumFont = [NSFontAttributeName : UIFont(name: "Raleway-Medium", size: CGFloat(22.0))!, NSForegroundColorAttributeName : UIColor.yellow, NSBackgroundColorAttributeName: UIColor.clear]
}

struct SystemTools {
    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

func dateToString(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    return formatter.string(from: date)
}

func parseHTMLString(_ string: String) -> NSAttributedString? {
    if let htmlData = string.data(using: .unicode) {
        do {
            let attributedString = try NSMutableAttributedString(data: htmlData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            attributedString.addAttributes(AttributesForString.yellowLightFont, range: NSMakeRange(0, attributedString.string.characters.count))
            return attributedString
        } catch {
            print("error \(error)")
        }
    }
    return nil
}
