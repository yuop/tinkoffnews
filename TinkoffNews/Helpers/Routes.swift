//
//  Routes.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation

struct Routes {
    static let mainRoute = "https://api.tinkoff.ru/v1/news"
    struct AdditionRoutes {
        static func postWith(id: String) -> String {
            return Routes.mainRoute + "_content?id=\(id)"
        }
        static func postsFrom(_ from: Int, to: Int) -> String {
            return Routes.mainRoute + "?first=\(from)&last=\(to)"
        }
    }
}
