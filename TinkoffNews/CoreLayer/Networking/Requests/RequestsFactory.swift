//
//  RequestsFactory.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import SwiftyJSON
struct RequestFactory {
    struct News {
        static func getNews(from begin: Int, to end: Int) -> RequestConfig<JSON> {
            let config = RequestConfig<JSON>(request: NewsJSONRequest(from: begin, to: end), parser: JSONParser())
            return config
        }
        static func getPost(with id: String) -> RequestConfig<JSON> {
            return RequestConfig<JSON>(request: PostJSONRequest(id: id), parser: JSONParser())
        }
    }
}
