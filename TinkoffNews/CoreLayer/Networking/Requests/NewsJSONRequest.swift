//
//  NewsJSONRequest.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
protocol IRequest: class {
    var urlRequest: URLRequest? {get}
}

class NewsJSONRequest: IRequest {
    
    
    var limitFrom: Int = 0
    var limitTo: Int = 10
    
    
    var urlRequest: URLRequest? {
        let url = Routes.AdditionRoutes.postsFrom(limitFrom, to: limitTo)
        if let url = URL(string: url) {
            return URLRequest(url: url)
        }
        return nil
    }
    
    init(from begin: Int, to end: Int) {
        self.limitFrom = begin
        self.limitTo = end
    }
}
