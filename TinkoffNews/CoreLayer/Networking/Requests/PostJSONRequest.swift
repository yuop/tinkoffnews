//
//  PostJSONRequest.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 22.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation


class PostJSONRequest: IRequest {
    
    
    var id: String
    
    
    var urlRequest: URLRequest? {
        let url = Routes.AdditionRoutes.postWith(id: id)
        if let url = URL(string: url) {
            return URLRequest(url: url)
        }
        return nil
    }
    
    init(id: String) {
        self.id = id
    }
}
