//
//  JSONParser.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import SwiftyJSON
class Parser<T> {
    func parse(data:Data) -> T? { return nil }
}

class JSONParser: Parser<JSON> {
    override func parse(data:Data) -> JSON? {
        return JSON(data: data)
    }
}
