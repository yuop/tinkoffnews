//
//  RequestSender.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation

struct RequestConfig<T> {
    let request: IRequest
    let parser: Parser<T>
}
enum Result<T> {
    case success(T)
    case fail(String)
}

protocol IRequestSender: class {
    func send<T>(config: RequestConfig<T>, completionHandler: @escaping (Result<T>)->Void)
}
class RequestSender: IRequestSender {
    func send<T>(config: RequestConfig<T>, completionHandler: @escaping (Result<T>) -> Void) {
        guard let request = config.request.urlRequest else {
            assertionFailure()
            return
        }
        URLSession.shared.dataTask(with: request, completionHandler: {data, response, error in
            if let error = error {
                completionHandler(Result.fail(error.localizedDescription))
            }
            
            guard let data = data, let parsedModel = config.parser.parse(data: data) else {
                completionHandler(Result.fail("parsing error"))
                return
            }
            completionHandler(Result.success(parsedModel))
        }).resume()
    }

    
}
