//
//  Syncer.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 21.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON
protocol ISyncDataObject {
    func syncList(json: JSON, in context: NSManagedObjectContext,  completion: @escaping ()->Void, onFail: @escaping ()->Void)
    func syncObject(with id: String, json: JSON, in context: NSManagedObjectContext,  completion: @escaping (String)->Void, onFail: @escaping ()->Void)
}
class Syncer: ISyncDataObject {
    
    func syncList(json: JSON, in context: NSManagedObjectContext, completion: @escaping ()->Void, onFail:@escaping ()->Void) {
        
        var _arrayIDs: [[String:String]]?
        
        do {
            _arrayIDs = try context.fetch(Post.fetchRequestIDs()) as? [[String:String]]
            guard let array = json.array, let arrayIDs = _arrayIDs else {
                assertionFailure("json is not an array")
                return
            }
            
            for object in array {
                for arrayID in arrayIDs {
                    if arrayID["id"] == object["id"].stringValue {
                        if Post.updatePost(from: object, with: arrayID["id"]!, in: context) == nil {
                            onFail()
                        }
                    }
                }
                if !arrayIDs.contains(where: {
                    return $0["id"] == object["id"].stringValue
                }) {
                    if Post.addNewPost(from: object, in: context) == nil {
                        onFail()
                    }
                }
            }
            completion()

        } catch {
            onFail()
        }
    }
    
    func syncObject(with id: String, json: JSON, in context: NSManagedObjectContext,  completion: @escaping (String)->Void, onFail: @escaping ()->Void) {
        
        guard let post = Post.findPost(with: id, context: context), let content = json["payload"]["content"].string else {
            assertionFailure("can't find content in json")
            return
        }
        context.perform {
            post.content = content
            completion(content)
        }
    }

    
}
