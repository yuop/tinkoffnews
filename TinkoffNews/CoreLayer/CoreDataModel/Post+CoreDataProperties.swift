//
//  Post+CoreDataProperties.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 21.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post")
    }
    public class func fetchRequestIDs() -> NSFetchRequest<NSDictionary> {
        let fetchRequest = NSFetchRequest<NSDictionary>(entityName: "Post")
        fetchRequest.propertiesToFetch = ["id"]
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.returnsDistinctResults = true
        return fetchRequest
    }

    @NSManaged public var content: String?
    @NSManaged public var id: String
    @NSManaged public var publicationDate: Int64
    @NSManaged public var seenTimes: Int32
    @NSManaged public var text: String


    
    static func fetchRequestPost(withID id: String, model: NSManagedObjectModel) -> NSFetchRequest<Post>? {
        let templateName = "PostWithId"
        guard let fetchRequest = model.fetchRequestFromTemplate(withName: templateName, substitutionVariables: ["id" : id]) as? NSFetchRequest<Post>? else {
            assert(false, "No template with name \(templateName)!")
            return nil
        }
        return fetchRequest
    }
    
    private static func deleteFetchRequest() -> NSBatchDeleteRequest{
            let delAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Post"))
        return delAllReqVar
    }
    
    static func deleteAllPosts(in context: NSManagedObjectContext, onCompletion: @escaping ()->(), onFail: @escaping ()->()) {
        do {
            let objects = try context.fetch(Post.fetchRequest()) as! [Post]
            for object in objects {
                context.delete(object)
            }
            onCompletion()
        } catch {
            print("error deleting: \(error)")
            onFail()
        }
    }
    
    static func findPost(with id: String, context: NSManagedObjectContext) -> Post? {
        guard let model = context.persistentStoreCoordinator?.managedObjectModel, let request = fetchRequestPost(withID: id, model: model) else {
            assertionFailure("can't find model")
            return nil
        }
        do {
            return try context.fetch(request).first
        } catch {
            print("error: \(error)")
            return nil
        }
    }
    
    private static func setDataFrom(json: JSON, to post: Post) {
        guard let id = json["id"].string,
            let text = json["text"].string,
            let publicationDate = json["publicationDate"]["milliseconds"].int64 else {
                assertionFailure("fail in parsing json for new Post")
                return
        }
        post.id = id
        post.text = text
        post.publicationDate = publicationDate
    }
    
    static func addNewPost(from json: JSON, in context: NSManagedObjectContext) -> Post? {
        var newPost: Post?
            if let post = NSEntityDescription.insertNewObject(forEntityName: "Post", into: context) as? Post {
                setDataFrom(json: json, to: post)
                newPost = post
            }
        return newPost
    }
    static func updatePost(from json: JSON,with id: String, in context: NSManagedObjectContext) -> Post? {
        guard let model = context.persistentStoreCoordinator?.managedObjectModel else {
            assertionFailure("there is no model")
            return nil
        }
        var updatedPost: Post?
        guard let fetchRequest = Post.fetchRequestPost(withID: id, model: model) else {
            assertionFailure("there is no fetchRequest")
            return nil
        }
            do {
                let results = try context.fetch(fetchRequest)
                assert(results.count < 2,"few news with same id")
                if let foundPost = results.first {
                    updatedPost = foundPost
                    setDataFrom(json: json, to: updatedPost!)
                }
            } catch {
                print("error while fetching: \(error)")
            }
        return updatedPost
    }
    
    
}
