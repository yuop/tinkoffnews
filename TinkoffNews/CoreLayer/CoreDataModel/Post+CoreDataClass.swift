//
//  Post+CoreDataClass.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 21.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import CoreData


public class Post: NSManagedObject {
    public override var description: String {
        return "id: \(id), publicationDate: \(publicationDate), text: \(text)"
    }
    
    
    public func getTimeDate() -> Date? {
        
        return Date(timeIntervalSince1970: TimeInterval(self.publicationDate/1000))
    
    }
    
    public func addViewing(in context: NSManagedObjectContext) {
        context.perform {
            self.seenTimes += 1
            do {
                try context.save()
            } catch {
                print("context save error: \(error)")
            }
        }
    }
}
