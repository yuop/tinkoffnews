//
//  NewsCell.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var counter: UILabel!
    
    func setupCell(withObject post: Post) {
        guard let date = post.getTimeDate() else {
            assertionFailure("can't parse date")
            return
        }
        self.title.attributedText = parseHTMLString(post.text)
        let time = dateToString(date: date)
        self.dateTime.text = time
        self.counter.text = "\(post.seenTimes)"
    }
    
    
    func setupCell(title: String,dateTime: Date) {
    
        self.title.attributedText = parseHTMLString(title)
        self.dateTime.text = dateToString(date: dateTime)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
