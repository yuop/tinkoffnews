//
//  PostViewController.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 22.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {
    
    @IBOutlet weak var internetErrorLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var model: IModel!
    var id: String!
    var content: NSAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
        
        if let content = content {
            self.textView.attributedText = content
        } else {
            downloadContentFromInternet(id: id)
        }
        
    }
    
    func downloadContentFromInternet(id: String) {
        if SystemTools.isInternetAvailable() {
                indicator.startAnimating()
                DispatchQueue.global(qos: .background).async {
                    self.model.downloadPost(with: id, onCompletion: { content in
                        DispatchQueue.main.async {
                            self.textView.attributedText = parseHTMLString(content)
                            self.indicator.stopAnimating()
                        }
                    }, onFail: {
                        DispatchQueue.main.async {
                            self.indicator.stopAnimating()
                            self.textView.isHidden = true
                        }
                    })
                }
        } else {
            self.indicator.stopAnimating()
            self.textView.isHidden = true
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        model.updatePostCounter(with: id, onCompletion: {})

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
