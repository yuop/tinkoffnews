//
//  NewsTableViewController.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import UIKit
import SwiftyJSON



class NewsTableViewController: UITableViewController {

    @IBOutlet weak var pagingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pullLabel: UILabel!
    
    var model: IModel!
    var dataProvider: INewsDataProvider!
    var page = 0
    var limit = 20
    var thatsIt = false
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupRefreshControl()
        initModel()
        setupDataProvider()
        setupDeleteButton()
        hideHeader()
        dataProvider.getData()
        downloadPullToRefresh()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initModel() {
        let newsService = NewsService(requestSender: RequestSender())
        model = Model(newsService: newsService)
        model.delegate = self
    }
    func setupTableView() {
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = RefreshControlTitles.loading
        
        refreshControl.tintColor = .yellow
        refreshControl.addTarget(self, action: #selector(self.downloadPullToRefresh), for: .valueChanged)
        self.refreshControl = refreshControl
        self.tableView.contentOffset = CGPoint(x:0, y:-self.refreshControl!.frame.size.height)
        
    }
    
    func setupDeleteButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "trashIcon"), style: .plain, target: self, action: #selector(alertDeleteData))
        self.navigationItem.leftBarButtonItem?.tintColor = .yellow
    }
    
    func setupDataProvider() {
        guard let mainContext = model.storageService.getMainContext() else {
            assertionFailure("main context doesnt exist")
            return
        }
        self.dataProvider = NewsDataProvider(tableView: self.tableView, context: mainContext)
    }
    
    func alertDeleteData() {
        let alert = UIAlertController(title: "Удаление данных", message: "Вы уверены, что хотите удалить сохраненные новости?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Да", style: .destructive, handler: { action in
            self.refreshControl?.attributedTitle = nil
            self.refreshControl?.beginRefreshing()
            self.model.deleteAllCachedData(onCompletion: {
                self.page = 0
                DispatchQueue.main.async {
                    self.stopIndicators()
                    self.showHeader()
                }
            }, onFail: {
                print("error occured while deleteting data")
            })
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func downloadPullToRefresh() {
        self.refreshControl?.attributedTitle = RefreshControlTitles.loading
        if !self.refreshControl!.isRefreshing {
            self.refreshControl!.beginRefreshing()
        }
        
        if SystemTools.isInternetAvailable() {
            hideHeader()
            if !thatsIt {
                page = 1
                    self.model.downloadNews(from: (self.page-1)*self.limit, to: self.page*self.limit)
            }
        } else {
            DispatchQueue.main.async {
                self.refreshControl?.attributedTitle = RefreshControlTitles.networkError
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
                if self.refreshControl!.isRefreshing {
                    self.refreshControl!.endRefreshing()
                }
            })
        }
    }
    
    func showHeader() {
        self.tableView.tableHeaderView?.frame.size.height = 44
        pullLabel.isHidden = false
    }
    func hideHeader() {
        if self.tableView.tableHeaderView?.frame.size.height != 0 {
            self.tableView.tableHeaderView?.frame.size.height = 0
            pullLabel.isHidden = true
        }
    }
    
    func downloadPaging() {
        if SystemTools.isInternetAvailable() {
            if !thatsIt && !isLoading {
                isLoading = true
                page += 1
                pagingIndicator.startAnimating()
                DispatchQueue.global(qos: .background).async {
                    self.model.downloadNews(from: (self.page-1)*self.limit, to: self.page*self.limit)
                }
            }
        }
    }
    
    func stopIndicators() {
        self.pagingIndicator.stopAnimating()
        if self.refreshControl!.isRefreshing {
            self.refreshControl?.endRefreshing()
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        guard let sections = self.dataProvider.fetchedResultsController.sections else {
            assertionFailure("there is no sections")
            return 0
        }
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.dataProvider.fetchedResultsController.sections else {
            assertionFailure("there is no sections")
            return 0
        }
        return sections[section].numberOfObjects
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsCell
        let object = self.dataProvider.fetchedResultsController.object(at: indexPath)
        cell.setupCell(withObject: object)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let id = self.dataProvider.fetchedResultsController.object(at: indexPath).id
//        model.updatePostCounter(with: id, onCompletion: {
//            print("counter updated")
//        })
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (limit * page) - 5 && !thatsIt {
            downloadPaging()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? PostViewController,
        let cell = sender as? UITableViewCell,
        let index = tableView.indexPath(for: cell) else {
            assertionFailure("error while prepare for segue")
            return
        }
        vc.model = model
        let object = self.dataProvider.fetchedResultsController.object(at: index)
        vc.id = object.id
        if let content = object.content {
            vc.content = parseHTMLString(content)
        }
    }


}

extension NewsTableViewController: IModelDelegate {
    func setup(objectsCount: Int) {
        isLoading = false
        if objectsCount < self.limit {
            self.thatsIt = true
        }
        DispatchQueue.main.async {
            self.stopIndicators()
        }
    }
    func show(error message: String) {
        print(message)
        DispatchQueue.main.async {
            self.stopIndicators()
        }
    }
}
