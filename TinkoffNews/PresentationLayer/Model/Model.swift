//
//  Model.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import SwiftyJSON
import Sync

//enum DataType {
//    case listPost
//    case fullPost
//}

protocol IModel: class {
    weak var delegate: IModelDelegate? {get set}
    var storageService: IStorageService { get }
    func downloadNews(from begin: Int, to end: Int)
    func downloadPost(with id: String, onCompletion: @escaping (String)->(), onFail: @escaping ()->())
    func updatePostCounter(with id: String, onCompletion: @escaping ()->())
    func deleteAllCachedData(onCompletion: @escaping ()->(), onFail: @escaping ()->())
}

protocol IModelDelegate: class {
    func setup(objectsCount: Int)
    func show(error message: String)
}

class Model: IModel {
    weak var delegate: IModelDelegate?
    private let newsService: INewsService
    let storageService: IStorageService
    var newsUpdater: INewsUpdater!
    
    
    init(newsService: INewsService){
        self.newsService = newsService
        let storageParent = StorageService()
        self.storageService = storageParent
        self.newsUpdater = storageParent
    }
    
    func deleteAllCachedData(onCompletion: @escaping ()->(), onFail: @escaping ()->()) {
        newsUpdater.deleteAllCachedData(onCompletion: onCompletion, onFail: onFail)
    }
    
    func updatePostCounter(with id: String, onCompletion: @escaping ()->()) {
        newsUpdater.updatePostCount(with: id, completion: onCompletion)
    }
    
    func downloadNews(from begin: Int, to end: Int) {
        newsService.getNews(from: begin, to: end, completionHandler: { json, error in
            if let json = json {
                self.storageService.syncFrom(json: json["payload"], onCompletion: {
                    print(json["payload"])
                    print("YEAH")
                    self.delegate?.setup(objectsCount: json["payload"].count)

                }, onFail: { error in
                    print(error)
                })
            } else {
                self.delegate?.show(error: error ?? "some error")
            }
        })
    }
    func downloadPost(with id: String, onCompletion: @escaping (String)->(), onFail: @escaping ()->()) {
        newsService.getPost(with: id, completionHandler: { json, error in
            if let json = json {
                self.storageService.syncPost(with: id, json: json, onCompletion: { content in
                    onCompletion(content)
                }, onFail:onFail)
            } else {
                onFail()
            }
        })
    }
}
