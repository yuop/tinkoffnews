//
//  NewsService.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 18.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import SwiftyJSON
protocol INewsService: class {
    func getNews(from begin: Int, to end: Int, completionHandler: @escaping(JSON?,String?)->Void)
    func getPost(with id: String, completionHandler: @escaping(JSON?,String?)->Void)
}

class NewsService: INewsService {
    let requestSender: IRequestSender
    let dispatchQueue = DispatchQueue(label: "com.tinkoffNews.serialQueue")
    init(requestSender: IRequestSender) {
        self.requestSender = requestSender
    }
 
    func getNews(from begin: Int, to end: Int, completionHandler: @escaping (JSON?, String?) -> Void) {
        dispatchQueue.async {
            let config = RequestFactory.News.getNews(from: begin, to: end)
            self.requestSender.send(config: config, completionHandler: { result in
                switch result {
                case .success(let news):
                    completionHandler(news,nil)
                case .fail(let error):
                    completionHandler(nil,error)
                }
            })
        }
    }
    
    func getPost(with id: String, completionHandler: @escaping (JSON?, String?) -> Void) {
        dispatchQueue.async {
            let config = RequestFactory.News.getPost(with: id)
            self.requestSender.send(config: config, completionHandler: { result in
                switch result {
                case .success(let news):
                    completionHandler(news,nil)
                case .fail(let error):
                    completionHandler(nil,error)
                }
            })
        }
    }
    
}
