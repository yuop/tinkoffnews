//
//  StorageService.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 20.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import SwiftyJSON
import Sync

protocol IStorageService: class {
    func syncFrom(json: JSON, onCompletion: @escaping ()->(), onFail: @escaping ()->())
    func syncPost(with id: String, json: JSON, onCompletion: @escaping (String)->(), onFail: @escaping ()->())
    func getMainContext() -> NSManagedObjectContext?
    func getSaveContext() -> NSManagedObjectContext?
    func performSave(completionHandler: (()->())? )}

protocol INewsUpdater: class {
    func updatePostCount(with id: String, completion: @escaping ()->())
    func deleteAllCachedData(onCompletion: @escaping ()->(), onFail: @escaping ()->())
}


class StorageService: IStorageService, INewsUpdater {
    let coreDataStack: ICoreDataStack
    let syncer: ISyncDataObject
    init() {
        self.coreDataStack = CoreDataStack()
        self.syncer = Syncer()
    }
    
    
    func syncFrom(json: JSON, onCompletion: @escaping () -> (), onFail: @escaping () -> ()) {
        guard let saveContext = coreDataStack.saveContext else {
            assertionFailure("Save context not found")
            return
        }
        saveContext.perform {
            
            self.syncer.syncList(json: json, in: saveContext, completion: {
                self.performSave(completionHandler: {
                    onCompletion()
                })
            }, onFail:  onFail)
        }
    }
    func syncPost(with id: String, json: JSON, onCompletion: @escaping (String) -> (), onFail: @escaping () -> ()) {
        guard let saveContext = coreDataStack.saveContext else {
            assertionFailure("Save context not found")
            return
        }
        saveContext.perform {
            self.syncer.syncObject(with: id,json: json, in: saveContext, completion: { content in
                self.performSave(completionHandler: {
                 onCompletion(content)
                })
            }, onFail:  onFail)
        }
    }
    func updatePostCount(with id: String, completion: @escaping ()->()) {
        guard let saveContext = coreDataStack.saveContext else {
            assertionFailure("Save context not found")
            return
        }
        saveContext.performAndWait {
            guard let post = Post.findPost(with: id, context: saveContext) else {
                assertionFailure("there is no post")
                return
            }
            post.seenTimes += 1
        }
        
        performSave(completionHandler: completion)
        
    }
    
    func deleteAllCachedData(onCompletion: @escaping ()->(), onFail: @escaping ()->()) {
        guard let saveContext = coreDataStack.saveContext else {
            assertionFailure("Save context not found")
            return
        }
        saveContext.perform {
            Post.deleteAllPosts(in: saveContext, onCompletion: {
                self.performSave(completionHandler: onCompletion)
            }, onFail: onFail )
        }
    }
    
    
    func getMainContext() -> NSManagedObjectContext? {
        return coreDataStack.mainContext
    }
    func getSaveContext() -> NSManagedObjectContext? {
        return coreDataStack.saveContext
    }
    func performSave(completionHandler: (() -> ())?) {
        guard let saveContext = coreDataStack.saveContext else {
            assert(false,"Save context not found")
            return
        }
        coreDataStack.performSave(context: saveContext, completionHandler: completionHandler)
    }
    
}
