//
//  NewsDataProvider.swift
//  TinkoffNews
//
//  Created by Savelyev Yura on 20.06.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
//
//  ConversationDataProvider.swift
//  TinkoffChat
//
//  Created by Savelyev Yura on 08.05.17.
//  Copyright © 2017 Savelyev Yura. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol INewsDataProvider: class {
    func objectsCount() -> Int
    var fetchedResultsController: NSFetchedResultsController<Post> { get }
    var tableView: UITableView { get }
    func getData()
}

class NewsDataProvider: NSObject, INewsDataProvider {
    let fetchedResultsController: NSFetchedResultsController<Post>
    
    internal let tableView: UITableView
    
    init(tableView: UITableView, context: NSManagedObjectContext) {
        self.tableView = tableView
        let fetchRequest: NSFetchRequest<Post> = Post.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key:"publicationDate",ascending: false)]
        fetchRequest.fetchBatchSize = 20
        fetchedResultsController = NSFetchedResultsController<Post>(fetchRequest:
            fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        super.init()
        fetchedResultsController.delegate = self
    }
    func objectsCount() -> Int {
        return try! self.fetchedResultsController.managedObjectContext.count(for: Post.fetchRequest())
    }
    func getData() {
        do {
            try self.fetchedResultsController.performFetch()
            print("COUNT \(self.fetchedResultsController.fetchedObjects?.count)")
        } catch {
            print("Error fetching: \(error)")
        }
    }
    
}
extension NewsDataProvider: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

        tableView.beginUpdates()
        
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .none)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .none)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .none)
            }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .delete :
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .move,.update: break
        }
    }
    
    
    
    
}
